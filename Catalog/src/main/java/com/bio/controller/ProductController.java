package com.bio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bio.bean.Product;
import com.bio.dto.ProductDTO;
import com.bio.service.ProductService;
import com.bio.util.Message;
import com.bio.util.Response;

import javassist.NotFoundException;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/{id}")
	public Response<Product> getProduct(@PathVariable long id) {
		Response<Product> response = new Response<>();
		Optional<Product> optionalProduct = productService.findById(id);
		if (optionalProduct.isPresent()) {
			response.setData(optionalProduct.get());
			response.setMessage(Message.PRODUCT_FOUND);
			response.setStatus(HttpStatus.FOUND);
		} else {
			response.setMessage(Message.PRODUCT_NOT_FOUND);
			response.setStatus(HttpStatus.NOT_FOUND);
		}
		return response;
	}

	@PostMapping("/add")
	public Response<Product> addUser(@Valid @RequestBody ProductDTO productDTO,BindingResult result) {
		Response<Product> response = new Response<>();
		try {
			StringBuilder builder =new StringBuilder();
			if (result.hasErrors()) {
				response.setStatus(HttpStatus.NOT_ACCEPTABLE);
				List<FieldError> errors = result.getFieldErrors();
				for (FieldError error : errors) {
					builder.append(error.getObjectName() + " - " + error.getDefaultMessage()+"##");
				}
				response.setMessage(builder.toString());
			}else {
				Product newBean = productService.save(productDTO);
				response.setData(newBean);
				response.setMessage(Message.PRODUCT_SAVE);
				response.setStatus(HttpStatus.CREATED);
			}
		} catch (NotFoundException e) {
			response.setMessage(e.getMessage());
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			response.setMessage(Message.PRODUCT_NOT_SAVE);
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
		}
		return response;
	}
}
