package com.bio.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bio.bean.Attribute;
import com.bio.dto.AttributeDTO;
import com.bio.service.AttributeService;
import com.bio.util.Message;
import com.bio.util.Response;

import javassist.NotFoundException;

@RestController
@RequestMapping("attribute")
public class AttributeController {

	@Autowired
	private AttributeService attributeService;
	
	@PostMapping("/add")
	public Response<Attribute> save(@Valid @RequestBody AttributeDTO attributeDTO) {
		Response<Attribute> response = new Response<>();
		try {
			Attribute newBean = attributeService.save(attributeDTO);
			response.setData(newBean);
			response.setStatus(HttpStatus.CREATED);
			response.setMessage(Message.ATTRIBUTE_SAVE);
		}
		catch (Exception e) {
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
			response.setMessage(Message.ATTRIBUTE_NOT_SAVE);
		}
		return response; 
	}
	
	@GetMapping("/category/{category_id}")
	public Response<Set<Attribute>> save(@PathVariable long category_id) {
		 Response<Set<Attribute>> response = new Response<>();
		try {
			Set<Attribute> data = attributeService.findByCategoryId(category_id);
			if(data.size()>0) {
				response.setData(data);
				response.setStatus(HttpStatus.FOUND);
				response.setMessage(Message.ATTRIBUTE_FOUND);
			}else {
				response.setStatus(HttpStatus.NOT_FOUND);
				response.setMessage(Message.ATTRIBUTE_NOT_FOUND);
			}
		}catch (NotFoundException e) {
			response.setStatus(HttpStatus.NOT_FOUND);
			response.setMessage(Message.CATEGORY_NOT_FOUND);
		}
		catch (Exception e) {
			response.setStatus(HttpStatus.NOT_FOUND);
			response.setMessage(Message.ATTRIBUTE_NOT_FOUND);
		}
		return response; 
	}
	
}
