package com.bio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bio.bean.Category;
import com.bio.service.CategoryService;
import com.bio.util.Message;
import com.bio.util.Response;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@PostMapping("/add")
	public Response<Category> save(@Valid @RequestBody Category category, BindingResult bindingResult) {
		Response<Category> response = new Response<>();
		try {
			if (bindingResult.hasErrors()) {
				response.setStatus(HttpStatus.NOT_ACCEPTABLE);
				List<FieldError> errors = bindingResult.getFieldErrors();
				for (FieldError error : errors) {
					response.setMessage(error.getObjectName() + " - " + error.getDefaultMessage());
				}
			}else {
				Category newCategory = categoryService.save(category);
				response.setData(newCategory);
				response.setStatus(HttpStatus.CREATED);
				response.setMessage(Message.CATEGORY_SAVE);
			}
		} catch (Exception e) {
			response.setStatus(HttpStatus.NOT_ACCEPTABLE);
			response.setMessage(Message.CATEGORY_NOT_SAVE);
		}
		return response;
	}

	@GetMapping("/{id}")
	public Response<Category> get(@PathVariable long id) {
		Response<Category> response = new Response<>();
		Optional<Category> newCategory = categoryService.findById(id);
		if (newCategory.isPresent()) {
			response.setData(newCategory.get());
			response.setStatus(HttpStatus.FOUND);
			response.setMessage(Message.CATEGORY_FOUND);
		} else {
			response.setStatus(HttpStatus.NOT_FOUND);
			response.setMessage(Message.CATEGORY_NOT_FOUND);
		}
		return response;
	}

}
