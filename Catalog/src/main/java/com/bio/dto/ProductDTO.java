package com.bio.dto;

import java.util.Set;

public class ProductDTO {

	private String name;
	private float price;
	private long categoryId;
	private Set<Long> attributeId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public Set<Long> getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Set<Long> attributeId) {
		this.attributeId = attributeId;
	}

}
