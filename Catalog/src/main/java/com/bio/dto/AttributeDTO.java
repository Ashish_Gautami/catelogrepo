package com.bio.dto;

public class AttributeDTO {

	private long id;

	private String name;

	private String value;

	private long categoryId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long category) {
		this.categoryId = category;
	}

}
