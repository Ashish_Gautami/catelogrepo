package com.bio.service;

import java.util.List;
import java.util.Optional;

import com.bio.bean.Category;

public interface CategoryService {

	public List<Category> findAll();

	public Category save(Category category);

	public Optional<Category> findById(long id);

}
