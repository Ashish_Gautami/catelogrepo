package com.bio.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.bio.bean.Attribute;
import com.bio.dto.AttributeDTO;

import javassist.NotFoundException;

public interface AttributeService {

	public List<Attribute> findAll();

	public Attribute save(AttributeDTO attribute) throws NotFoundException;

	public Optional<Attribute> findById(long id);

	public Set<Attribute> findByCategoryId(long id) throws NotFoundException;
	
	public Set<Attribute> findAttributeIn(Set<Long> ids);
	
}
