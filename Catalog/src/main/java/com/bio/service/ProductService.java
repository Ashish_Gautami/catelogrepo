package com.bio.service;

import java.util.List;
import java.util.Optional;

import com.bio.bean.Product;
import com.bio.dto.ProductDTO;

import javassist.NotFoundException;

public interface ProductService {
	public List<Product> findAll();

	public Product save(ProductDTO ProductDTO) throws NotFoundException;

	public Optional<Product> findById(long id);

}
