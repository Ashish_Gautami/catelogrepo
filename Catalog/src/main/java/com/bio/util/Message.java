package com.bio.util;


public final class Message {

	public static final String PRODUCT_FOUND = "Product is found successfully !";
	public static final String PRODUCT_NOT_FOUND = "Opps! your product is not found!";
	public static final String PRODUCT_SAVE = "Product is found successfully saved!";
	public static final String PRODUCT_NOT_SAVE = "Opps! your product is not save!";

	public static final String CATEGORY_FOUND = "category is found successfully !";
	public static final String CATEGORY_NOT_FOUND = "Opps! your category is not found!";
	public static final String CATEGORY_SAVE = "category is found successfully saved!";
	public static final String CATEGORY_NOT_SAVE = "Opps! your category is not save!";

	public static final String ATTRIBUTE_FOUND = "attribute is found successfully !";
	public static final String ATTRIBUTE_NOT_FOUND = "Opps! your attribute is not found!";
	public static final String ATTRIBUTE_SAVE = "attribute is found successfully saved!";
	public static final String ATTRIBUTE_NOT_SAVE = "Opps! your attribute is not save!";

	
}
