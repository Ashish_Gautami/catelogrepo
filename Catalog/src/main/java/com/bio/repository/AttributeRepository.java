package com.bio.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bio.bean.Attribute;
import com.bio.bean.Category;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {

	Set<Attribute> findByCategory(Category category);
	Set<Attribute> findByIdIn(Set<Long> ids);
}
