package com.bio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bio.bean.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	
	
}
