package com.bio.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bio.bean.Category;
import com.bio.repository.CategoryRepository;
import com.bio.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository repository;

	@Override
	public List<Category> findAll() {
		return repository.findAll();
	}

	@Override
	public Category save(Category category) {
		return repository.save(category);
	}

	@Override
	public Optional<Category> findById(long id) {
		return repository.findById(id);
	}

}
