package com.bio.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bio.bean.Attribute;import com.bio.bean.Category;
import com.bio.dto.AttributeDTO;
import com.bio.repository.AttributeRepository;
import com.bio.service.AttributeService;
import com.bio.service.CategoryService;
import com.bio.util.Message;

import javassist.NotFoundException;

@Service
public class AttributeServiceImpl implements AttributeService {

	@Autowired
	private AttributeRepository repository;
	
	@Autowired
	private CategoryService categoryService;
	
	@Override
	public List<Attribute> findAll() {
		return repository.findAll();
	}

	@Override
	public Attribute save(AttributeDTO attribute) throws NotFoundException {
		if(categoryService.findById(attribute.getCategoryId()).isPresent()) {
			Attribute attribute2 =new Attribute();
			Category category = new Category();
			category.setId(attribute.getCategoryId());
			attribute2.setCategory(category);
			attribute2.setName(attribute.getName());
			attribute2.setValue(attribute.getValue());
			return repository.save(attribute2);
		}else {
			throw new NotFoundException(Message.CATEGORY_NOT_FOUND);
		}
	}

	@Override
	public Optional<Attribute> findById(long id) {
		return repository.findById(id);
	}

	@Override
	public Set<Attribute> findByCategoryId(long id) throws NotFoundException {
		if(categoryService.findById(id).isPresent()) {
			Category category = new Category();
			category.setId(id);
			return repository.findByCategory(category);
		}else {
			throw new NotFoundException(Message.CATEGORY_NOT_FOUND);
		}
	}

	@Override
	public Set<Attribute> findAttributeIn(Set<Long> ids) {
		return repository.findByIdIn(ids);
	}


}
