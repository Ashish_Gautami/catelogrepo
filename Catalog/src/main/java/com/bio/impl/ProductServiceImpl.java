package com.bio.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bio.bean.Attribute;
import com.bio.bean.Category;
import com.bio.bean.Product;
import com.bio.dto.ProductDTO;
import com.bio.repository.ProductRepository;
import com.bio.service.AttributeService;
import com.bio.service.CategoryService;
import com.bio.service.ProductService;
import com.bio.util.Message;

import javassist.NotFoundException;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private AttributeService attributeService;

	@Override
	public List<Product> findAll() {
		return repository.findAll();
	}

	@Override
	public Product save(ProductDTO productDTO) throws NotFoundException {
		if (categoryService.findById(productDTO.getCategoryId()).isPresent()) {
			Set<Attribute> allAtrributes = new HashSet<Attribute>();
			Set<Attribute> attributes = attributeService.findAttributeIn(productDTO.getAttributeId());
			for(Attribute bean:attributes) {
				if (productDTO.getAttributeId().contains(bean.getId())) {
					Attribute attribute = new Attribute();
					attribute.setId(bean.getId());
					allAtrributes.add(attribute);
				}
			}
			if (allAtrributes.size() == productDTO.getAttributeId().size()) {
				Category category = new Category();
				category.setId(productDTO.getCategoryId());

				Product product = new Product();
				product.setAttribute(allAtrributes);
				product.setCategory(category);
				product.setName(productDTO.getName());
				product.setPrice(productDTO.getPrice());
				return repository.save(product);
			}else {
				throw new NotFoundException(Message.ATTRIBUTE_NOT_FOUND);
			}

		} else {
			throw new NotFoundException(Message.CATEGORY_NOT_FOUND);
		}

	}

	@Override
	public Optional<Product> findById(long id) {
		return repository.findById(id);
	}

}
